﻿#include <iostream>


int main()
{
    int sum = 0;
    int i, j, k;
    char b[6];
    for (i = 1; i <= 999999; i++)
    {
        for (k = i, j = 0; j < 6; j++, k /= 10) {
            b[j] = k % 10;
        }
        if (b[0] + b[1] + b[2] == b[3] + b[4] + b[5]) {
            sum+=1;
        }
    }
    std::cout << sum << std::endl;
    return 0;
}

